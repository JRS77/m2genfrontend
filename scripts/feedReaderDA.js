function GetLists()
{
	var lists = [];
	lists.push(new ListType('iTunes Top 100 Albums', 'ituneAlbums'));
	lists.push(new ListType('iTunes Top 100 Audiobooks', 'ituneAudiobooks'));
	return lists;
};

function ListType(listName, listValue)
{
	this.name = listName,
	this.value = listValue
};

function GetListItems(listName, searchString)
{
	//var listItems = [];
	var listMaker;
	
	if(listName == 'ituneAlbums')
	{
		listMaker = new ituneAlbumsList();
	}
	else if(listName == 'ituneAudiobooks')
	{
		listMaker = new ituneAudiobooksList();
	}
	
	var listItems = listMaker.listItems;
	
	if(searchString && searchString.length > 0)
	{
		searchString = searchString.toLowerCase();
		var filteredListItems = [];
		
		for (i = 0; i < listItems.length; ++i)
		{
			if(listItems[i].line1.toLowerCase().indexOf(searchString) >= 0 || listItems[i].line2.toLowerCase().indexOf(searchString) >= 0)
			{
				filteredListItems.push(listItems[i]);
			}
		}
		listItems = filteredListItems;
	}
	
	return listItems;
}

function ListItem(rank, line1, line2, imageUrl)
{
	this.rank = rank,
	this.line1 = line1,
	this.line2 = line2,
	this.imageUrl = imageUrl
};

var ituneAlbumsList = function() {
	var listItems = [];
	$.ajax({
	 url: 'https://itunes.apple.com/us/rss/topalbums/limit=100/json',
     dataType: 'json',
     async: false,
     success: function(data) {
		for(var i in data.feed.entry){
			var li = new ListItem(parseInt(i) + 1, 
				data.feed.entry[i]['im:name'].label, 
				data.feed.entry[i]['im:artist'].label, 
				data.feed.entry[i]['im:image'][2].label);
			listItems.push(li);
		}
	 }
	});
	this.listItems = listItems;
};

var ituneAudiobooksList = function() {
	var listItems = [];
	$.ajax({
	 url: 'https://itunes.apple.com/us/rss/topaudiobooks/limit=100/json',
     dataType: 'json',
     async: false,
     success: function(data) {
		for(var i in data.feed.entry){
			var li = new ListItem(parseInt(i) + 1, 
				data.feed.entry[i]['im:name'].label, 
				data.feed.entry[i]['im:artist'].label, 
				data.feed.entry[i]['im:image'][2].label);
			listItems.push(li);
		}
	 }
	});
	this.listItems = listItems;
};